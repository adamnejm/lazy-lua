
--- Constraints a value within a range
---@param value number Value to constrain
---@param min number Minimum value
---@param max number Maximum value
---@return number
local function clamp(value, min, max)
	return math.min(math.max(value, min), max)
end

--- Constrains an integer within range using modulo
--- Credits: Karmastan https://stackoverflow.com/a/3057867
---@param value integer Value to constrain
---@param min number Minimum value
---@param max number Maximum value
local function wrap(value, min, max)
	return ((value - min) % (max - min + 1)) + min
end

--- Remaps a value from one range to another
---@param value number Value to remap
---@param in_min number Lower bounds of the initial range
---@param in_max number Upper bounds of the initial range
---@param out_min number Lower bounds of the target range
---@param out_max number Upper bounds of the target range
---@return number
local function map(value, in_min, in_max, out_min, out_max)
	return out_min + (((value - in_min) / (in_max - in_min)) * (out_max - out_min))
end

--- Rounds the value to the nearest interval
---@param value number Value to snap
---@param interval integer Interval to snap to
---@return number
local function snap(value, interval)
	return math.floor(value / interval + 0.5) * interval
end

--- Performs a linear interpolation between two values
---@param ratio number Ratio in range [0..1]
---@param from number Beginning
---@param to number End
---@return number
local function lerp(ratio, from, to)
	return from + (to - from) * ratio
end

--- Rounds a number to an integer or decimal point if the second argument is used
---@param value number to round
---@param decimals? integer Optional decimal place to round to, otherwise it'll round to nearest integer
---@return number
local function round(value, decimals)
	local mul = decimals and (10 ^ decimals) or 1
	return math.floor(value * mul + 0.5) / mul
end

--- Strips fraction from a float
---@param value number Number to truncate
---@param decimals? integer Number of decimal places to keep
local function truncate(value, decimals)
	local mul = decimals and (10 ^ decimals) or 1
	if value < 0 then
		return math.ceil(value * mul) / mul
	else
		return math.floor(value * mul) / mul
	end
end

--- Returns a random float within [min, max) range
---@param min number Minimum value
---@param max number Maximum value
---@return number
local function rand(min, max)
	return min + (max - min) * math.random()
end

--- Returns sign of the number
---@param value number
---@param no_zero? boolean If true, it will return 1 for 0 and greater, -1 otherwise
---@return number sign Either 1, -1 or 0
local function sign(value, no_zero)
	if no_zero then
		if value < 0 then
			return -1
		else
			return 1
		end
	else
		if value > 0 then
			return 1
		elseif value < 0 then
			return -1
		else
			return 0
		end
	end
end

return {
	clamp    = clamp,
	wrap     = wrap,
	map      = map,
	snap     = snap,
	lerp     = lerp,
	round    = round,
	truncate = truncate,
	rand     = rand,
	sign     = sign,
}
