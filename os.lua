
--- Executes a command and returns its output
---@param command string Command to execute
---@param format? boolean True to trim and replace newlines with spaces
---@return string? result Result of the command
local function capture(command, format)
	local handle = io.popen(command)
	if handle then
		local result = handle:read("*a")
		handle:close()
		
		if format then
			result = string.gsub(result, "[\n\r]+", "")
			return string.match(result, "^%s*(.-)%s*$") or result
		else
			return result
		end
	end
end

--- Returns a table containing the given time split into the following units: days, hours, minutes, seconds
---@param seconds number Time in seconds
---@return table units Table with fields `d`, `h`, `m`, `s`
local function time_units(seconds)
	return {
		s = math.floor(seconds % 60),
		m = math.floor(seconds % 3600 / 60),
		h = math.floor(seconds % 86400 / 3600),
		d = math.floor(seconds / 86400),
	}
end

-------------------------------------------

return {
	capture    = capture,
	time_units = time_units,
}
