
local table_unpack = table.unpack or unpack ---@diagnostic disable-line deprecated

-------------------------------------------

--- Splits string on separator into an array
--- Credits: Facepunch https://github.com/Facepunch/garrysmod/blob/master/garrysmod/lua/includes/extensions/string.lua
---@param str string String to turn into a table
---@param separator? string Optional Separator to split by or nil to split every each letter
---@param enable_patterns? boolean Optional option to accept Lua patterns in the separator, False by default
---@return string[]
local function split(str, separator, enable_patterns)
	local result = {}
	if not separator or separator == "" then
		for i = 1, string.len(str) do
			result[i] = string.sub(str, i, i)
		end
	else
		local current_pos = 1
		for i = 1, string.len(str) do
			local start_pos, finish_pos = string.find(str, separator, current_pos, not enable_patterns)
			if not start_pos then break end
			
			result[i] = string.sub(str, current_pos, start_pos - 1)
			current_pos = finish_pos + 1
		end
		
		result[#result+1] = string.sub(str, current_pos)
	end
	return result
end

--- Removes whitespaces from beginning and end of a string
---@param str string
---@return string
local function trim(str)
	return string.match(str, "^%s*(.-)%s*$") or str
end

--- Checks whether a string begins with the given prefix
---@param str string
---@param prefix string Prefix to compare
local function starts_with(str, prefix)
	return string.sub(str, 1, string.len(prefix)) == prefix
end

--- Checks whether a string ends with the given suffix
---@param str string
---@param suffix string Suffix to compare
local function ends_with(str, suffix)
	return string.sub(str, -string.len(suffix)) == suffix
end

-------------------------------------------

--- Searches for patterns in the given string
---@param str string
---@param patterns string[] String patterns to find
---@return boolean? # True if any pattern was found in the string
local function find_any(str, patterns)
	for _, pattern in ipairs(patterns) do
		if string.find(str, pattern) then
			return true
		end
	end
end

--- Turns a number into a string with appropriate sign at the beginning, eg. `+43` or `-25`
---@param number number
---@param positive_zero boolean Whether `0` should have a sign, ie. `+0`
local function sign(number, positive_zero)
	if positive_zero then
		return number == 0 and "+0" or number < 0 and tostring(number) or "+" .. number
	else
		return number == 0 and "0" or number < 0 and tostring(number) or "+" .. number
	end
end

--- Returns number's ordinal suffix, ie. `st`, `nd`, `rd` or `th`
--- By default the number `0` returns `th`, use the optional `zero_ordinal` parameter to control it
---@param number integer
---@param zero_ordinal? string | false Custom ordinal for the number `0` or `false` to return empty string
---@return string ordinal
local function ordinal(number, zero_ordinal)
	local str = tostring(number)
	if number == 0 and zero_ordinal ~= nil then
		return zero_ordinal == false and "" or zero_ordinal
	elseif ends_with(str, "11") or ends_with(str, "12") or ends_with(str, "13") then
		return "th"
	elseif ends_with(str, "1") then
		return "st"
	elseif ends_with(str, "2") then
		return "nd"
	elseif ends_with(str, "3") then
		return "rd"
	else
		return "th"
	end
end

--- Pluralizes a word by appending the letter `s` to it
---@param value number Value used to determine whether the word should be plural
---@param noun string Word to pluralize
---@return string
---@overload fun(noun: string): string
local function plural(value, noun)
	if noun then
		return value ~= 1 and noun .. "s" or noun
	else
		return value .. "s"
	end
end

--- Adds quotation marks around an object
---@param value any Value to quote
---@return string quoted_str String in quotation marks
local function quoted(value)
	return "\"" .. tostring(value) .. "\""
end

-------------------------------------------

--- Returns Levenshtein distance between two strings
--- Credits: https://gist.github.com/Badgerati/3261142
---@param str1 string Word to compare
---@param str2 string Word to compare
---@param case_insensitive? boolean True to not take letter case into consideration, false by default
---@return number distance
local function leven(str1, str2, case_insensitive)
	if case_insensitive then
		str1, str2 = string.lower(str1), string.lower(str2)
	end
	
	local len1, len2 = #str1, #str2
	if len1 == 0 then
		return len2
	elseif len2 == 0 then
		return len1
	elseif str1 == str2 then
		return 0
	end
	
	local matrix = {}
	for i = 0, len1, 1 do
		matrix[i] = {}
		matrix[i][0] = i
	end
	for j = 0, len2, 1 do
		matrix[0][j] = j
	end
	
	for i = 1, len1, 1 do
		for j = 1, len2, 1 do
			local cost = string.byte(str1, i) == string.byte(str2, j) and 0 or 1
			matrix[i][j] = math.min(matrix[i-1][j] + 1, matrix[i][j-1] + 1, matrix[i-1][j-1] + cost)
		end
	end
	
	return matrix[len1][len2]
end

--- Performs a fuzzy matching for the pattern against a string and returns a score if it matched
--- Credits: Jonathan Coates @SquidDev - https://github.com/SquidDev-CC/metis/blob/dev/src/metis/string/fuzzy.lua
---@param str string Haystack to search in
---@param pattern string Pattern to search for
---@param adjacency_bonus? number Optional score bonus for correctly matching adjacent letters, 5 by default
---@param leading_penalty? number Optional score penalty per missed letter leading up to the match, -2 by default
---@param unmatched_penalty? number Optional score penalty for not matching a letter, -1 by default
---@return number? score Score of the match (can be negative) or nil if not matched anything
local function fuzzy(str, pattern, adjacency_bonus, leading_penalty, unmatched_penalty)
	adjacency_bonus   = adjacency_bonus   or 5
	leading_penalty   = leading_penalty   or -2
	unmatched_penalty = unmatched_penalty or -1
	
	local str_lower, pattern_lower = string.lower(str), string.lower(pattern)
	local str_length, pattern_length = string.len(str), string.len(pattern)
	
	if pattern_length == 0 then
		return str_length == 0 and 0 or nil
	end
	
	local start = 1
	local best_score
	while true do
		start = string.find(str_lower, string.sub(pattern_lower, 1, 1), start, true)
		if not start then break end
		
		local score = math.max(leading_penalty * (start - 1), -start)
		local previous_match = true
		local str_pos, pattern_pos = start + 1, 2
		
		while str_pos <= str_length and pattern_pos <= pattern_length do
			local str_char = string.sub(str_lower, str_pos, str_pos)
			local pattern_char = string.sub(pattern_lower, pattern_pos, pattern_pos)
			
			if pattern_char == str_char then
				if previous_match then
					score = score + adjacency_bonus
				end
				
				previous_match = true
				pattern_pos = pattern_pos + 1
			else
				score = score + unmatched_penalty
				previous_match = false
			end
			
			str_pos = str_pos + 1
		end
		
		if pattern_pos > pattern_length and (not best_score or score > best_score) then
			best_score = score
		end
		
		start = start + 1
	end
	
	return best_score
end

--- Performs a fuzzy search with the given pattern on a number of strings and returns a table sorted by the highest score and alphabetical order
---@param pattern string Fuzzy pattern to search for
---@param elements string[] Array of strings to perform the search on
---@return string[] found Array of strings sorted by fuzzy algorithm score
local function search(pattern, elements)
	local found, scores = {}, {}
	
	for _, v in ipairs(elements) do
		local score = fuzzy(v, pattern)
		if score then
			scores[v] = score
			found[#found+1] = v
		end
	end
	
	table.sort(found, function(a, b)
		local score_a, score_b = scores[a], scores[b]
		if score_a == score_b then
			return a < b
		else
			return score_a > score_a
		end
	end)
	
	return found
end

-------------------------------------------

return {
	split       = split,
	trim        = trim,
	starts_with = starts_with,
	ends_with   = ends_with,
	
	find_any    = find_any,
	sign        = sign,
	ordinal     = ordinal,
	plural      = plural,
	quoted      = quoted,
	leven       = leven,
	fuzzy       = fuzzy,
	search      = search,
}
