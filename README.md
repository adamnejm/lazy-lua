> ### <mark>ARCHIVED!</mark>
> <mark>**Current repository at Codeberg: https://codeberg.org/adamnejm/lazy-lua**

# **Lazy** - Extended Standard Library for Lua *>= 5.1*
*"Why waste time say lot word when few word do trick"*

---

## Usage

**Lazy** automatically handles relative requires of its submodules.  
Thanks to that it can be imported from virtually anywhere, even from outside of the project's directory.  
  
Assuming that **Lazy** is located in `./lib/lazy` you would require it like so:  
```lua
local lazy = require "lib.lazy"
lazy.string.split("Hello World!", " ") --> { "Hello", "World!" }
```

You may also require individual modules at will:  
```lua
local lmath = require "lib.lazy.math"
lmath.round(19.86) --> 20
```

To make **Lazy** importable from anywhere on your system, you can set `LUA_PATH` variable:  
```sh
LUA_PATH="$HOME/projects/lua/lib/?.lua;$HOME/projects/lua/lib/?/init.lua;;"
```
*You can safely export this in your `.profile` (or equivalent) to make it persistent.*  
It'll allow you to import **Lazy** and other libraries located in `$HOME/projects/lua/lib` without having to copy or link the **Lazy** folder into the project's directory.  

## Documentation
Every function is documented using [sumneko's LSP](https://github.com/LuaLS/lua-language-server) annotation system.  
Meaning you can get dynamic syntax checking and autocompletion in [any code editor](https://microsoft.github.io/language-server-protocol/implementors/tools/) that supports the Language Server Protocol.
